<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
 function __construct() {
        parent::__construct();
         $this->load->library('form_validation');
		 $this->load->helper('url');
		 $this->load->library('session');
		 $this->load->library('mail');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('login.php');
	}
	public function Userlogin()
	{

		$email = $this->input->post('email');
		$password = $this->input->post('password');
	    $this->form_validation->set_rules('email', 'Email', 'required'); 
		$this->form_validation->set_rules('password', 'Password', 'required');	
        if ($this->form_validation->run() == FALSE) { 
        $this->load->view('login.php'); 
        }
      else
       {

	$this->db->select('*');
	$this->db->from('userlogin');
	$this->db->where("email", $email);
    $this->db->where("password", $password);
    $query = $this->db->get();
    //echo $this->db->last_query();die;
	$result = $query->result_array();
	$sessionsetID = $result[0]['uname'];
	//rint_r($result);die;

	$rowcount = $query->num_rows();
	
	if($rowcount > 0)
	{
	 $this->session->set_userdata('setvalue',$sessionsetID);
	 $this->load->view('userDashboard.php'); 	
	} else
	{
	$this->session->set_flashdata('error','Invalid email or password');
	$this->load->view('login.php'); 
	}
       }	 
        
	}
	public function allUsers()
	{
		$this->load->view('allusers.php');
	}

	public function addUsers()
	{
		$this->load->view('addusers.php');
	}
	public function logout()
	{
		$this->session->unset_userdata('setvalue');
		redirect('/');
	}
	public function Usermailsend()
	{
     $data = $this->mail->sendmail();
	//	echo "aa";
	}
	public function UserSubmission()
	{
 $username = $_POST['username'];
 $email = $_POST['email'];
 $emaiformate = filter_var($email, FILTER_SANITIZE_EMAIL);
 $password = $_POST['password'];
 $cpassword = $_POST['cpassword'];
 $phone = $_POST['phone'];

 if($username == '')
 {
     echo "Please Fill Name";
 }
 elseif (filter_var($emaiformate, FILTER_VALIDATE_EMAIL) === false || $emaiformate != $email ) {
    echo "Please Enter Valid Email ID";
 }
  elseif($password == '')
 {
     echo "Please Enter New Password";
 }
   elseif($cpassword == '')
 {
     echo "Please Enter Confirm Password";
 }
    elseif($cpassword != $password)
 {
     echo "Your Password Not Match";
 }
   elseif($phone == '')
 {
     echo "Please Enter Phone Number";
 }

 else {

 $curdate = date('d-m-Y-h:i:sa');
 $test = "$curdate-".$_FILES["profileImg"]["name"];
 //echo $test;die;

 $ext = explode(".", $test);
 $exttype = $ext[1];
 $arr = array('jpg','jpeg');


 if (in_array("$exttype", $arr))
 {
 //echo $filename = filesize($_FILES["profileImg"]["tmp_name"]);die;
 $imagedetails = getimagesize($_FILES["profileImg"]["tmp_name"]);
 //print_r($imagedetails);die;
 $width = $imagedetails[0];
 $height = $imagedetails[1];
 if($width >= 400 || $height >= 400)
 {
     echo "Please Upload Pic 400X400";
 } else {

 $location = '../upload/'. $test;
 move_uploaded_file($_FILES["profileImg"]["tmp_name"], $location); 
 echo "Upload Successfully.";
 } } else {
 echo "Upload File Invalid Extention!";
 } } 
	}

public function createUser()
{
	$name = $_POST['name'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$number = $_POST['number'];
	$message = $_POST['message'];
	//$picture = $_FILES['picture'];

$config['upload_path'] = './upload';
$config['allowed_types'] = 'gif|jpg|png';
$this->load->library('upload', $config);
$this->upload->initialize($config);
$this->upload->do_upload('picture');
$upload_data=$this->upload->data('picture');



	$data = array('uname'=>$name,
		           'email'=>$email,
		           'password'=>$password,
		           'number'=>$number,
		           'message'=>$message);
  $data1 = $this->db->insert('userlogin', $data);
  $this->session->set_flashdata('usermessage', 'User Create Successfully.', 2);
   redirect('welcome/addUsers');

  
}
    

}
