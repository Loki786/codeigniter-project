

<!DOCTYPE html>
<html lang="en" class="loading">
  <head>
    <?php include('include/topcss.php'); ?>
  </head>
  <body data-col="1-column" class=" 1-column  blank-page blank-page" >
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="wrapper">
      <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper"><!--Login Page Starts-->
<section id="login">
    <div class="container-fluid">
        <div class="row full-height-vh">
            <div class="col-12 d-flex align-items-center justify-content-center">
                <div class="card gradient-indigo-purple text-center width-400">
                   
                    <div class="card-body">
                        <div class="card-block">
                            <h2 class="white">Login</h2>
                           
                            <div class="errorcoler"><?php echo $this->session->flashdata('error'); ?></div>
                            <form method="POST" action="<?php echo base_url(); ?>index.php/welcome/Userlogin">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input type="email" class="form-control" name="email" placeholder="Email" >
                                        <div class="errorcoler"><?php echo form_error('email'); ?></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input type="password" class="form-control" name="password" placeholder="Password">
                                    </div>
                                    <div class="errorcoler"><?php echo form_error('password'); ?></div>
                                </div>

                               

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" value="submit" class="btn btn-pink btn-block btn-raised ">Login</button>
                                       
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-left"><a (click)="onForgotPassword()" class="white">Recover Password</a></div>
                        <div class="float-right"><a href="register.php" class="white">New User?</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Login Page Ends-->
          </div>
        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    <!-- BEGIN VENDOR JS-->
   <?php include('include/bottomjs.php'); ?>
    <!-- END APEX JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
  </body>
</html>

<style type="text/css">
  .errorcoler{
    color: red;
  }
</style>