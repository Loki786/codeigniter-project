
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.js"></script> 


        <script type="text/javascript">
            $('document').ready(function(){

      $('#form').validate({
                    rules:{
                        "name":{
                            required:true,
                            maxlength:40
                        },

                        "email":{
                            required:true,
                            email:true,
                            maxlength:100,
                        },

                        "password":{
                             required:true,
                             minlength: 5,
                        },
           
                        "cpassword":{
                               required:true,
                               equalTo:"#password",
                        },

                         "number":{
                               required:true,
                                minlength:10,
                                maxlength:11,
                        },

                        "message":{
                            required:true
                        },
                       "picture":{
                            required:true
                        }},

                    messages:{
                        "name":{
                            required:"Name field is required"
                        },

                        "email":{
                            required:"Email field is required",
                            email:"Please enter a valid email address"
                        },

                        "password":{
                            required:"Password field is required",
                        },

                        "cpassword":{
                            required:"Confirm Password field is required",
                            
                        },

                         "number":{
                            required:"Phone Number field is required",
                            
                        },

                        "message":{
                            required:"Message field is required"
                        },
                       "picture":{
                            required:"Picture field is required"
                        }},

                  
                
            })
            
        });
        </script> 
    <style type="text/css">
    .error{
    
    color:#FF0000;
    
    }
    </style>
</head>

<body>
<?php echo $this->session->flashdata('usermessage'); ?>
<form  name="form" id="form" action="<?php echo base_url(); ?>index.php/welcome/createUser" method="post" enctype="multipart/form-data">
<br />
<table>
  <tr>
    <td>Name:</td>
    <td><input type="text" name="name" class="" maxlength="40"></td>
    </tr>
  <tr>
    <td> Email:</td>
    <td><input type="text" name="email" class="" maxlength="40" ></td>
    </tr>
     <tr>
    <td> Password:</td>
    <td><input type="text" name="password" id="password" class="" maxlength="40" ></td>
    </tr>
     <tr>
    <td> Confirm Password:</td>
    <td><input type="text" name="cpassword" class="" maxlength="40" ></td>
    </tr>
      <tr>
    <td> Number:</td>
    <td><input type="text" name="number" class="" maxlength="10" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" ></td>
    </tr>
  <tr>
    <td> Message:</td>
    <td><input type="text" name="message" class="" maxlength="40" ></td>
    </tr>
     <tr>
    <td> Picture:</td>
    <td><input type="file" name="picture"></td>
   
    </tr>
</table>

<input type="submit" name="submit" value="Submit" >
</form>
</body>
</html>

<script>
$(document).ready(function(){
  $("#number").keypress(function(e){
     var keyCode = e.which;
    /*
      8 - (backspace)
      32 - (space)
      48-57 - (0-9)Numbers
    */
 
    if ( (keyCode != 8 || keyCode ==32 ) && (keyCode < 48 || keyCode > 57)) { 
      return false;
    }
  });
});
</script>
<script>
//When the page has loaded.
$( document ).ready(function(){
$('#message12').fadeIn('slow', function(){
$('#message12').delay(1000).fadeOut(); 
});
});
</script>
<script>
//When the page has loaded.
$( document ).ready(function(){
$('#message13').fadeIn('slow', function(){
$('#message13').delay(1000).fadeOut(); 
});
});
</script>